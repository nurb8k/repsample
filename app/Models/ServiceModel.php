<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServiceModel extends Model
{
    use HasFactory;
use TCG\Voyager\Traits\Translatable;

class ServiceModel extends Model
{
    use Translatable;

    protected $fillable = [
        'slug',
        'name',
        'description',
        'img',
    ];
    public $timestamps = false;

    protected $locale;

    protected $translatable = ['name', 'description'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->locale = app()->getLocale();
    }

    public function getNameTranslatedAttribute($value)
    {
        return $this->getTranslatedAttribute('name', "$this->locale");
    }

    public function getDescTranslatedAttribute($value)
    {
        return $this->getTranslatedAttribute('description', "$this->locale");
    }


}
