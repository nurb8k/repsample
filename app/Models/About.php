<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class About extends Model
{
    use Translatable;
    protected $guarded = [];
    public $timestamps = false;
    protected $locale;

    protected $translatable = ['title', 'sub_title',
                                'description', 'item1_title',
                                'item2_title', 'item3_title',
                                'item4_title', 'item5_title',
                                'item6_title'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->locale = app()->getLocale();
    }

    public function getTitleTranslatedAttribute($value)
    {
        return $this->getTranslatedAttribute('title', "$this->locale");
    }

    public function getSubTitleTranslatedAttribute($value)
    {
        return $this->getTranslatedAttribute('sub_title', "$this->locale");
    }


    public function getDescTranslatedAttribute($value)
    {
        return $this->getTranslatedAttribute('description', "$this->locale");
    }

    public function getItem1TitleTranslatedAttribute()
    {
        return $this->getTranslatedAttribute('item1_title', "$this->locale");
    }

    public function getItem2TitleTranslatedAttribute()
    {
        return $this->getTranslatedAttribute('item2_title', "$this->locale");
    }

    public function getItem3TitleTranslatedAttribute()
    {
        return $this->getTranslatedAttribute('item3_title', "$this->locale");
    }

    public function getItem4TitleTranslatedAttribute()
    {
        return $this->getTranslatedAttribute('item4_title', "$this->locale");
    }

    public function getItem5TitleTranslatedAttribute()
    {
        return $this->getTranslatedAttribute('item5_title', "$this->locale");
    }

    public  function getItem6TitleTranslatedAttribute()
    {
        return $this->getTranslatedAttribute('item6_title', "$this->locale");
    }

}
