<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
    use HasFactory;
    use Illuminate\Database\Eloquent\Model;
    use TCG\Voyager\Traits\Translatable;

class Partner extends Model
{
    use Translatable;
    protected $locale;

    public $timestamps = false;
    protected $fillable = ['title', 'img', 'description'];
    protected $translatable = ['title', 'description'];
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->locale = app()->getLocale();
    }

    public function getTitleTranslatedAttribute($value)
    {
        return $this->getTranslatedAttribute('title', app()->getLocale());
    }

    public function getDescriptionTranslatedAttribute($value)
    {
        return $this->getTranslatedAttribute('description', app()->getLocale());
    }
}
