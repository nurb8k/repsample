<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class Banner extends Model
{
   use Translatable;
   protected $guarded = [];
   public $timestamps = false;
   protected $locale;

    protected $translatable = [
        'text',
        'item1_title', 'item1_text',
        'item2_title', 'item2_text',
        'item3_title', 'item3_text'
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->locale = app()->getLocale();
    }

    public function getTextTranslatedAttribute()
    {
        return $this->getTranslatedAttribute('text', "$this->locale");
    }

    public function getItem1TitleTranslatedAttribute()
    {
        return $this->getTranslatedAttribute('item1_title', "$this->locale");
    }

    public function getItem2TitleTranslatedAttribute()
    {
        return $this->getTranslatedAttribute('item2_title', "$this->locale");
    }

    public function getItem3TitleTranslatedAttribute()
    {
        return $this->getTranslatedAttribute('item3_title', "$this->locale");
    }

    public function getItem1TextTranslatedAttribute()
    {
        return $this->getTranslatedAttribute('item1_text', "$this->locale");
    }

    public function getItem2TextTranslatedAttribute()
    {
        return $this->getTranslatedAttribute('item2_text', "$this->locale");
    }

    public function getItem3TextTranslatedAttribute()
    {
        return $this->getTranslatedAttribute('item3_text', "$this->locale");
    }

}
