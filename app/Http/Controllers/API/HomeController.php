<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\AboutResource;
use App\Http\Resources\BannerResource;
use App\Http\Resources\GalleryResource;
use App\Http\Resources\PartnerResource;
use App\Http\Resources\ProductResource;
use App\Http\Resources\ReviewResource;
use App\Http\Resources\ServiceModelResource;
use App\Http\Traits\APIResponsesTrait;
use App\Models\About;
use App\Models\Banner;
use App\Models\Gallery;
use App\Models\Partner;
use App\Models\Product;
use App\Models\Review;
use App\Models\ServiceModel;

class HomeController extends Controller
{
    use APIResponsesTrait;

    public function index()
    {
        $data = [
            'banners' => BannerResource::collection(Banner::all()),
            'about' => AboutResource::collection(About::all()),
            'services' => ServiceModelResource::collection(ServiceModel::take(3)->get()),
            'partners' => PartnerResource::collection(Partner::all()),
            'products' => ProductResource::collection(Product::take(3)->get()),
            'reviews' =>  ReviewResource::collection(Review::all()),
            'gallery' => GalleryResource::collection(Gallery::all()),
        ];

        return $this->successResponse($data);
    }
}
