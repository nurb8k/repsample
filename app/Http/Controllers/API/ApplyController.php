<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Application;
use Illuminate\Http\Request;

class ApplyController extends Controller
{
    public function store(Request $request)
    {
        try {
            $validated = $request->validate([
                'name' => 'required|min:3|max:255',
                'email' => 'required|email',
                'phone' => 'required',
                'message' => 'required',
            ],[
                'name.required' => 'Поле имя обязательно',
                'name.min' => 'Минимальная длина имени 3 символа',
                'name.max' => 'Максимальная длина имени 255 символов',
                'email.required' => 'Поле email обязательно',
                'email.email' => 'Введите корректный email',
                'phone.required' => 'Поле телефон обязательно',
                'message.required' => 'Поле сообщение обязательно',
            ]);

            Application::create($validated);

            // Mail send to admin
            // Mail::to(env('MAIL_FROM_ADDRESS'))->send(new ApplicationMail($validated));

            return response()->json([
                'success' => true,
                'message' => 'Ваша заявка успешно отправлена на обработку, благодарим за обращение!',
            ]);

        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Произошла ошибка, попробуйте позже',
            ]);
        }

    }
}
