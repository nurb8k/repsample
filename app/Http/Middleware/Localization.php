<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;
use function PHPUnit\Framework\isEmpty;

class Localization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // get accept language from header
        $preferredLanguage = $request->getPreferredLanguage();

        if (!in_array($preferredLanguage, ['en', 'ru', 'kz'])) {
            $preferredLanguage = 'ru';
        }
        app()->setLocale($preferredLanguage);

        return $next($request);
    }


}