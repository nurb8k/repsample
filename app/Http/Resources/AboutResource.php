<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AboutResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'title' => $this->title_translated,
            'subtitle' => $this->subtitle_translated,
            'sub_title' => $this->sub_title_translated,
            'description' => $this->desc_translated,
            'item1_title' => $this->item1_title_translated,
            'item2_title' => $this->item2_title_translated,
            'item3_title' => $this->item3_title_translated,
            'item4_title' => $this->item4_title_translated,
            'item5_title' => $this->item5_title_translated,
            'item6_title' => $this->item6_title_translated,
        ];
    }
}
