<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BannerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'text' => $this->text_translated,
            'image' => $this->image ? asset('storage/' . $this->image) : null,

            'item1_title' => $this->item1_title_translated,
            'item1_desc' => $this->item1_text_translated,

            'item2_title' => $this->item2_title_translated,
            'item2_desc' => $this->item2_text_translated,

            'item3_title' => $this->item3_title_translated,
            'item3_desc' => $this->item3_text_translated,
        ];
    }
}
