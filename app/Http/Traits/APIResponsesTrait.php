<?php


namespace App\Http\Traits;

trait   APIResponsesTrait
{
    public function successResponse($data)
    {
        return response()->json([
            'success' => true,
            'data' => $data
        ],200);
    }

    public function errorResponse($message, $code = 404)
    {
        return response()->json([
            'success' => false,
            'message' => $message
        ], $code);
    }
}