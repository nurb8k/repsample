<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banners', function (Blueprint $table) {
            $table->text('text');
            $table->string('image');

            $table->string('item1_title');
            $table->string('item1_text');
            $table->string('item1_icon');

            $table->string('item2_title');
            $table->string('item2_text');
            $table->string('item2_icon');

            $table->string('item3_title');
            $table->string('item3_text');
            $table->string('item3_icon');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banners');
    }
}
