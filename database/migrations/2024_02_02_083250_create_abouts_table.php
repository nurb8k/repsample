<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAboutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('abouts', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('sub_title');
            $table->text('description');
            $table->string('image');

            $table->string('item1_title');
            $table->string('item1_icon');

            $table->string('item2_title');
            $table->string('item2_icon');

            $table->string('item3_title');
            $table->string('item3_icon');

            $table->string('item4_title');
            $table->string('item4_icon');

            $table->string('item5_title');
            $table->string('item5_icon');

            $table->string('item6_title');
            $table->string('item6_icon');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('abouts');
    }
}
